import React, { useState } from 'react';
import { NavLink, Navbar, Links, Logo, Name, NavBtn, LinksPopup } from './styles.js';
import { FaBars, FaOut } from 'react-icons/fa';

function NavBar() {
  const [toggleNavLinks, setToggleNavLinks] = useState(false);
  const [dimensions, setDimensions] = React.useState({ 
    height: window.innerHeight,
    width: window.innerWidth
  })
  React.useEffect(() => {
    function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
}
    window.addEventListener('resize', handleResize)
    return _ => {
      window.removeEventListener('resize', handleResize)
}
  })

  return (
    <>
      <Navbar>
        <Logo />
        {dimensions.width > 650 ? (
        <Links>
          <NavLink href="/">HOME</NavLink>
          <NavLink href="/">HOME</NavLink>
          <NavLink href="/">HOME</NavLink>
          <NavLink href="/">HOME</NavLink>
          <NavLink href="/">HOME</NavLink>
        </Links>
        ) : (
          <NavBtn onClick={() => setToggleNavLinks(!toggleNavLinks)}>
            <FaBars/>
          </NavBtn>
        )}
      </Navbar>
        {toggleNavLinks && dimensions.width < 650 ? (
          <LinksPopup>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
          </LinksPopup>
        ) : ''}
    </>
  )
}

export default NavBar;