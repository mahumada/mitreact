import styled from "styled-components";

export const Circle = styled.div`
  border: .25vw solid gold;
  border-radius: 100%;
  position: absolute;
  z-index: 10000;
  opacity: 1;
  @media (max-width: 650px){
    display: none;
  }
`