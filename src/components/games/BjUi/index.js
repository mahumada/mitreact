import React from 'react';
import { Wrapper, UiChip, UiChipRenderer, ChipDelete } from './styles.js';
import addBet from '../../../store/actionCreators/addBet.js';
import store from '../../../store/reducers/store.js';
import setBetChips from '../../../store/actionCreators/setBetChips.js';

function BjUi({index}) {
  let bet = store.getState().BJchips;

  store.subscribe(()=>{
    bet = store.getState().BJchips;
    console.log(store.getState().BJchips)
  })
  const addChip = (prop) => {
    bet[index][prop] += 1;
    setBetChips(bet);
  }

  return (
    <Wrapper>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f1.png" onClick={()=>{addBet(index, 1); addChip("one")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f5.png" onClick={()=>{addBet(index, 5); addChip("five")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f10.png" onClick={()=>{addBet(index, 10); addChip("ten")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f25.png" onClick={()=>{addBet(index, 25); addChip("twentyfive")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f50.png" onClick={()=>{addBet(index, 50); addChip("fifty")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f100.png" onClick={()=>{addBet(index, 100); addChip("hundred")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f200.png" onClick={()=>{addBet(index, 200); addChip("twohundred")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f500.png" onClick={()=>{addBet(index, 500); addChip("fivehundred")}}/>
      </UiChip>
      <UiChip>
        <UiChipRenderer src="assets/fichas/f1000.png" onClick={()=>{addBet(index, 1000); addChip("thousand")}}/>
      </UiChip>
    </Wrapper>
  )
}

export default BjUi;
