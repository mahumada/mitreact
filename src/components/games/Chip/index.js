import React, {useState, useEffect} from 'react';
import { Wrapper, Info } from './styles.js';
import store from '../../../store/reducers/store.js';

const Chip = ({ index }) => {

  const [bet, setBet] = useState(0);
  const [lastBet, setLastBet] = useState(0);
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth
  });

  store.subscribe(()=>{
    if(store.getState().gameState === 'none' && store.getState().bet[index] !== bet){
      const currentBet = store.getState().bet[index];
      setLastBet(currentBet - bet);
      setBet(currentBet);
    }
  })

  React.useEffect(() => {
    function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
    }
    window.addEventListener('resize', handleResize)
    return _ => {
      window.removeEventListener('resize', handleResize)
    }
  })

  const style = (index == 0) ? {
    top: "30.5%",
    left: "8.4%"
  } : (index == 1) ? {
    top: "36%",
    left: "47.2%"
  } : {
    top: "30.5%",
    left: "85.8%"
  };

  const infostyle = (index == 0) ? {
    left: '1%',
    bottom: '49.5%'
  } : (index == 1) ? {
    left: '39.8%',
    bottom: '44%'
  } : {
    left: '78.5%',
    bottom: '49.5%'
  };

  if(bet && dimensions.width > 650){
    return (
      <>
        <Wrapper id="betChip"  style={style} src={`assets/fichas/f${lastBet}.png`}/>
        <Info style={infostyle}>Bet: {store.getState().bet[index]}</Info>
      </>
      )
  }else{
    return '';
  }
}

export default Chip;