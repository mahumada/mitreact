import styled from 'styled-components';

export const Btn = styled.button`
  font-size: 2.5vmax;
  color: white;
  background: black;
  border-radius: 2vw;
  transition: 1s;
  margin-bottom: .1%;
  position: relative;
  padding: .4rem 1rem .4rem 1rem;
  @media (max-width: 650px){
    display: none;
  }

  &:hover {
    color: gold;
    transform: scale(1.1);
    box-shadow: 1vh 1vh 2vh black;
  }
`