const cardReducer = (state = {
  cards: [],
  bet: [0, 0, 0],
  BJchips: [{
    "one": 0,
    "five": 0,
    "ten": 0,
    "twentyfive": 0,
    "fifty": 0,
    "hundred": 0,
    "twohundred": 0,
    "fivehundred": 0,
    "thousand": 0
  },{
    "one": 0,
    "five": 0,
    "ten": 0,
    "twentyfive": 0,
    "fifty": 0,
    "hundred": 0,
    "twohundred": 0,
    "fivehundred": 0,
    "thousand": 0
  },{
    "one": 0,
    "five": 0,
    "ten": 0,
    "twentyfive": 0,
    "fifty": 0,
    "hundred": 0,
    "twohundred": 0,
    "fivehundred": 0,
    "thousand": 0
  }],
  gameState: 'none',
  alert: {
    display: false,
    text: '',
    color: 'none'
  }
}, action) => {
  switch(action.type){

    case 'flipCard':
      if(state.cards.length > 0){
        state.cards.map(card => {
          if(card.index === action.payload.index){
            card.flipped = true;
          }
          return card;
        });
        return {
          ...state
        }
      }else{
        return {
          ...state
        }
      }

    case 'addCard':
      const newCards = [...state.cards, {info: action.payload.info, flipped: true}]
      return {
        ...state,
        cards: newCards
      }
    
    case 'resetCard':
      return {
        ...state,
        cards: []
      }

    case 'addBet':
      let newBet = state.bet;
      newBet[action.payload.index] += action.payload.amount; 
      return {
        ...state,
        bet: newBet
      }

    case 'setBetChips':
      return {
        ...state,
        BJchips: action.payload.chips
      }

    case 'setGameState':
      return {
        ...state,
        gameState: action.payload.state
      }

    case 'setAlert':
      return {
        ...state,
        alert: {
          display: action.payload.display,
          text: action.payload.text,
          color: action.payload.color
        }
      }

    default: return state;
    
  } 
}

export default cardReducer;