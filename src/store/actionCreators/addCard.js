import store from '../reducers/store'

const addCard = info => {
  store.dispatch({
    type: 'addCard',
    payload: {
      info
    }
  })
}

export default addCard;