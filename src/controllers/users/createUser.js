const createUser = (username, password, firstName, lastName, email, phone) => {
  const body = {
    username,
    password,
    firstName,
    lastName,
    email,
    phone
  }
  return fetch(`https://mitapi.herokuapp.com/api/users`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default createUser;