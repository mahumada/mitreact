const getOneUser = id => {
  return fetch(`https://mitapi.herokuapp.com/api/users/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err));
}

export default getOneUser;