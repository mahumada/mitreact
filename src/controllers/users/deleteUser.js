const deleteUser = id => {
  id += 'a';
  return fetch(`https://mitapi.herokuapp.com/api/users/${id}`, {
    method: 'DELETE'
  })
  .then(res => JSON.stringify(res))
  .catch(err => console.error(err));
}

export default deleteUser;