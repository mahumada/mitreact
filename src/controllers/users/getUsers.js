const getUsers = () => {
  return fetch('https://mitapi.herokuapp.com/api/users', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .catch(err => console.error(err));
}

export default getUsers;