const standBJ = (userId) => {
  const body = {
    userId
  }
  return fetch(`https://mitapi.herokuapp.com/api/blackjack/stand`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default standBJ;