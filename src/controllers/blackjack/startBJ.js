const startBJ = (userId, bet) => {
  const body = {
    userId,
    bet
  }
  return fetch(`https://mitapi.herokuapp.com/api/blackjack`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default startBJ;