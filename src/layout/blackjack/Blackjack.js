import React, { useState, useEffect } from 'react';
import Card from '../../components/games/Card';
import Navbar from '../../components/navigation/NavBar';
import Table from '../../components/games/Table';
import Chip from '../../components/games/Chip';
import store from '../../store/reducers/store';
import BjUi from '../../components/games/BjUi';
import AlertBar from '../../components/games/AlertBar';
import OptionButton from '../../components/games/OptionButton';
import * as api from '../../controllers';
import setGameState from '../../store/actionCreators/setGameState';
import addBet from '../../store/actionCreators/addBet';
import { GameView } from '../styles';
import setAlert from '../../store/actionCreators/setAlert';
import StatusBar from '../../components/games/StatusBar';
import addCard from '../../store/actionCreators/addCard';
import resetCard from '../../store/actionCreators/resetCard';
import setBetChips from '../../store/actionCreators/setBetChips';
import StaticCard from '../../components/games/StaticCard';
import HiddenBtn from '../../components/games/HiddenBtn';
import Marker from '../../components/games/Marker';

// addCard(0);
function Blackjack() { 

  const [cards, setCards] = useState(store.getState().cards);
  const [dealerCards, setDealerCards] = useState([]);
  const [currentValue, setCurrentValue] = useState(false);
  const [bet, setBet] = useState(0);
  const [premio, setPremio] = useState(0);
  const [dealerValue, setDealerValue] = useState(0);
  const [index, setIndex] = useState(1);
  const [newDealerCards, setNewDealerCards] = useState(0);
  const [currentIndex, setCurrentIndex] = useState(1);

  store.subscribe(()=>{
    setCards(store.getState().cards);
    setBet(store.getState().bet);
    console.log(store.getState().BJchips)
  })

  const id = '617bf9f796c1b323ea7c774c';

  const start = async(userId) => {
    setCards([]);
    console.log('chips', store.getState().BJchips);
    let response = (await api.startBJ(userId, store.getState().BJchips[1]));
    console.log(await response);
    if(!response['error'] && response !== undefined){
      console.log("CURRENT HAND", response.currentHand, "CARDS", store.getState().cards, "LOCAL", cards);
      await response.currentHand.map(c => {
        console.log('Added card', c, 'to store', store.getState().cards);
        addCard(c);
      })
      setGameState('started');
      setCurrentValue(response.currentHandValue);
      setPremio(0);
      setDealerValue(response.dealerHandValue);
      setDealerCards([...response.dealerHand]);
      console.log('DEALER', [...response.dealerHand]);
      setNewDealerCards(response.dealerHand.length);
    }else{
      setAlert({display: true, text: response['error'], color: 'red'});
    }
  }
  
  const hit = async(userId) => {
    let response = (await api.hitBJ(userId));
    console.log(await response);
    setCurrentValue(response.currentHandValue);
        console.log('Added card', response.currentHand[response.currentHand.length-1], 'to store', store.getState().cards);
    addCard(response.currentHand[response.currentHand.length-1]);
    setDealerCards([...response.dealerHand]);
    if(response.userIsBusted){
      setAlert({display: true, text: 'BUSTED!', color: 'red'});
      setGameState('finished');
      setPremio('0');
    }
  }

  const stand = async(userId) => {
    let response = (await api.standBJ(userId));
    console.log(await response);
    setGameState('finished');
    setAlert(response.dealerIsBusted ? {display: true, color: 'lightgreen', text: 'YOU WON!'} : {display: true, color: 'red', text: 'YOU LOST.'});
    if(response.dealerIsBusted){
      setPremio(2 * bet[1]);
    }
    setDealerValue(response.dealerHandValue);
    setNewDealerCards(response.dealerHand.length - newDealerCards);
    setDealerCards([...response.dealerHand]);
  }

  const restart = () => {
    setCurrentValue(''); 
    setPremio(''); 
    setGameState('none'); 
    setAlert({display: false}); 
    resetCard(); 
    setDealerCards([]); 
    setDealerValue('');
  }

  

  return (
    <>
      <Navbar />
      <div style={{width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
      <GameView>

        <Table />

        <StatusBar itemA={{title: "SALDO"}} itemB={{title: "APUESTA"}} itemC={{title: "PREMIO", content: premio}}/>
        
        { store.getState().gameState === 'none' ? (
          <BjUi index={index}/>
        ) : ''}

        <Chip index="0"/>
        <HiddenBtn width="7%" height="12%" top="30%" left="8%" clickFunction={()=>{setIndex(0)}} index="0"/>
        <Chip index="1"/>
        <HiddenBtn width="7%" height="12%" top="35.5%" left="46.65%" clickFunction={()=>{setIndex(1)}} index="1"/>
        <Chip index="2"/>
        <HiddenBtn width="7%" height="12%" top="30%" left="85.35%" clickFunction={()=>{setIndex(2)}} index="2"/>

        {(index === 0) ? (
          <Marker width="5.5%" height="10%" top="30.4%" left="8.4%"/>
        ) : (index === 1) ? (
          <Marker width="5.5%" height="10%" top="36%" left="47.2%"/>
        ) : (
          <Marker width="5.5%" height="10%" top="30.4%" left="85.8%"/>
        )}
        

        <div style={{zIndex: 900000, position: 'absolute', width: '30%', height: '35%', display: 'flex', flexDirection: 'column', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-evenly', bottom: '10%', right: 0}}>
          { store.getState().gameState === 'none' && bet ? (
            <OptionButton text="Start" clickFunction={()=>{start(id)}}/>
          ) : ''}
          { store.getState().gameState === 'started' ? (
            <React.Fragment>
              <OptionButton text="Hit" clickFunction={()=>{hit(id)}}/>
              <OptionButton text="Stand" clickFunction={()=>{stand(id)}}/>
            </React.Fragment>
          ) : ''}
          { store.getState().gameState === 'finished' ? (
            <OptionButton text="Restart" clickFunction={()=>{restart()}}/>
          ) : ''}
        </div>

        {currentValue ? (
          <>  
            <h1 style={{color: 'black', background: 'gold', position: 'absolute', fontSize: '2.5vw', bottom: '20%', textAlign: 'center', right: '46.75%', zIndex: "9999999", margin: 0, padding: '.05vw .5vw .05vw .5vw', borderRadius: '1.5vw', border: '.25vw solid black'}}>{currentValue}</h1>
            <h1 style={{color: 'black', background: 'gold', position: 'absolute', fontSize: '2.5vw', top: '15%', textAlign: 'center', right: '46.75%', zIndex: "9999999", margin: 0, padding: '.05vw .5vw .05vw .5vw', borderRadius: '1.5vw', border: '.25vw solid black'}}>{dealerValue}</h1>
          </>
        ) : ''}
        
        {cards ? cards.map((card, index) => {
          return (
            <StaticCard index={index} imgSrc={`${card.info.suite}/${card.info.text.toLowerCase()}`} flipped={false}/>
            )
          }) : ''}
          {dealerCards ? dealerCards.map((card, index) => {
          if(card !== null){
            return (
            <StaticCard index={index} imgSrc={`${card.suite}/${card.text.toLowerCase()}`} flipped={false} isDealer={true}/>
            )
          }else{
            return ''
          }
          }) : ''}
        <AlertBar />
      
      </GameView>
      </div>
    </>
  )
}

export default Blackjack;