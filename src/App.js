import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home, Blackjack, Error } from './layout/index.js';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/blackjack">
          <Blackjack />
        </Route>
        <Route path="*">
          <Error />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
