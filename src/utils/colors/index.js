import { dark, mainColor, darkColor, lightColor, light } from './palettes/palette1.js';

export { dark, mainColor, darkColor, lightColor, light };